package com.apiit.cb006700.grocery_store.service;

import com.apiit.cb006700.grocery_store.models.AccountstatusModel;
import com.apiit.cb006700.grocery_store.repo.AccountStatusRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountStatusService {
    private final AccountStatusRepo accountStatusRepo;

    @Autowired
    public AccountStatusService(AccountStatusRepo accountStatusRepo) {
        this.accountStatusRepo = accountStatusRepo;
    }

    public AccountstatusModel getAccountStatusById(String id){
        return accountStatusRepo.findById(id).orElse(null);
    }

}
