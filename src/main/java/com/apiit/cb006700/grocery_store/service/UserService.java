package com.apiit.cb006700.grocery_store.service;

import com.apiit.cb006700.grocery_store.models.UserModel;
import com.apiit.cb006700.grocery_store.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private static final String DELETED_USER_STATUS = "STAT03";

    private final UserRepo userRepository;
    private final UserTypeService userTypeService;
    private final AccountStatusService accountStatusService;


    @Autowired
    public UserService(UserRepo userRepository, UserTypeService userTypeService, AccountStatusService accountStatusService) {
        this.userRepository = userRepository;
        this.userTypeService = userTypeService;
        this.accountStatusService = accountStatusService;
    }

    public UserModel getUserbyId(String userId){
        return userRepository.findById(userId).orElse(null);
    }

    public boolean isUserExist(String emailAddress){
        return userRepository.existsByEmailAddress(emailAddress);
    }

    public boolean isUserValid(String emailAddress, String password){
        return userRepository.existsByEmailAddressAndPassword(emailAddress, password);
    }

    public List<UserModel> getAllUsers(){
        return userRepository.findAll();
}
    public UserModel getUserModelByEmail(String emailAddress){
        return userRepository.findUserModelByEmailAddress(emailAddress);
    }


    public boolean deleteUser(UserModel userModel){
        //TODO:Check for pending deliveries and any item in the cart before deleting
        userModel.setAccountstatus(accountStatusService.getAccountStatusById(DELETED_USER_STATUS));
        return saveUser(userModel);
    }

    public boolean saveUser(UserModel userModel){
        return userRepository.save(userModel) != null;
    }

}
