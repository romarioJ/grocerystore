package com.apiit.cb006700.grocery_store.service;

import com.apiit.cb006700.grocery_store.models.UsertypeModel;
import com.apiit.cb006700.grocery_store.repo.UserTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTypeService {
    private final UserTypeRepo userTypeRepo;

    @Autowired
    public UserTypeService(UserTypeRepo userTypeRepo) {
        this.userTypeRepo = userTypeRepo;
    }

    public UsertypeModel getUserTypeById(String id){
        return userTypeRepo.findById(id).orElse(null);
    }
}
