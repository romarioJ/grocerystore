package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.DiscountitemModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscountItemRepo extends JpaRepository<DiscountitemModel,String> {
//    List<DiscountitemModel> getAllByProductvariationVariatiomOD
}
