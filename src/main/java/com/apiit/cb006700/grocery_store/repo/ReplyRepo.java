package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.ReplyModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReplyRepo extends JpaRepository<ReplyModel,String> {
}
