package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.AccountstatusModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountStatusRepo extends JpaRepository<AccountstatusModel,String> {
    Optional<AccountstatusModel> getByAccountStatusId(String ID);
}
