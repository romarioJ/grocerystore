package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.SaleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleRepo extends JpaRepository<SaleModel,String> {
    List<SaleModel> getAllByUserUserId(String userID);
}
