package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.DeliveryaddressModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryAddressRepo extends JpaRepository<DeliveryaddressModel,String> {
    DeliveryaddressModel getBySaleSaleId(String saleID);
}
