package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.SalestatusModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleStatusRepo extends JpaRepository<SalestatusModel,String> {
}
