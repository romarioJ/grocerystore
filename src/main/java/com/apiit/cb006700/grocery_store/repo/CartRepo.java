package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.CartModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepo extends JpaRepository<CartModel,String> {
    boolean existsByUserUserId(String userId);
    CartModel findByUserUserId(String id);
}
