package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.ProductvariationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductVariationRepo extends JpaRepository<ProductvariationModel,String> {
    List<ProductvariationModel> getAllByProductProductId (String ID);
    ProductvariationModel getTopByProductProductId(String ID);
    boolean existsByProductProductIdAndColourAndSize (String Id, String colour, String size);
}
