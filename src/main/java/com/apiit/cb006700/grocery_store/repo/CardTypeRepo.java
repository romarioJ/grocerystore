package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.CardtypeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardTypeRepo extends JpaRepository<CardtypeModel,String> {
    CardtypeModel getByCardTypeId (String Id);
}
