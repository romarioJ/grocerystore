package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.CardpaymentsModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardPaymentsRepo extends JpaRepository<CardpaymentsModel,String> {
    //ToDo : How Should This Be Displayed
    CardpaymentsModel findBySaleSaleId(String id);
}
