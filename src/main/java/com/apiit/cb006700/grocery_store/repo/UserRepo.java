package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<UserModel,String> {
    boolean existsByEmailAddress (String emailAddress);
    boolean existsByEmailAddressAndPassword (String emailAddress, String password);
    UserModel findUserModelByEmailAddress (String emailAddress);
}
