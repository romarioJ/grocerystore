package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.CreditcardModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CreditCardRepo extends JpaRepository<CreditcardModel,String> {
    List<CreditcardModel> findAllByUserUserId(String Id);
}
