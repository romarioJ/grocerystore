package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepo extends JpaRepository<ProductModel,String> {
    List<ProductModel> getProductModelsByProductbrandProductBrandId (String brandID);
    boolean  existsByProductName(String name);
//    List<ProductModel> getProductModelsByProductcategoryProductCategoryId(String catID);
    List<ProductModel> getProductModelsBySubcategorySubCatID(String subCatID);
    List<ProductModel> getProductModelsBySubcategorySubCatIDAndProductbrandProductBrandId(String subCatID, String brandID);
//    List<ProductModel> getProductModelsByProductcategoryProductCategoryIdAndProductbrandProductBrandId
//            (String catID, String BrandID);
}
