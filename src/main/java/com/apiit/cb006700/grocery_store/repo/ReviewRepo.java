package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.ReviewModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepo extends JpaRepository<ReviewModel,String> {
}
