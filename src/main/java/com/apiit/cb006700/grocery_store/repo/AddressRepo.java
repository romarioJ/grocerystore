package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.AddressModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepo extends JpaRepository<AddressModel,String> {
    List<AddressModel> getAllByUserUserId(String userID);
}
