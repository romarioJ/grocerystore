package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.ProductbrandModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductBrandRepo extends JpaRepository<ProductbrandModel,String> {
    boolean existsByProductBrandDescContains(String desc);
}
