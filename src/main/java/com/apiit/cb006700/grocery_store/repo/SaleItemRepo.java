package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.SaleitemModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleItemRepo extends JpaRepository<SaleitemModel,String> {
    List<SaleItemRepo>getAllBySaleSaleId(String id);
}
