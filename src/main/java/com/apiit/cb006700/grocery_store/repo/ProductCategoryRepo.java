package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.ProductcategoryModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductCategoryRepo extends JpaRepository<ProductcategoryModel,String> {
    boolean existsByProductCatDescContains(String catDesc);
}
