package com.apiit.cb006700.grocery_store.repo;

import com.apiit.cb006700.grocery_store.models.CartitemModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartItemRepo extends JpaRepository<CartitemModel,String> {
    List<CartitemModel> getAllByCartCartId(String cartID);
    boolean removeAllByCartCartId(String cartID);
    Optional<CartitemModel> getCartitemModelByCartCartIdAndProductvariationVariationId(String cartID, String variationId);

}
