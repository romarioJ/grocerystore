package com.apiit.cb006700.grocery_store.controller;


import com.apiit.cb006700.grocery_store.DTO.UserDTO;
import com.apiit.cb006700.grocery_store.models.AccountstatusModel;
import com.apiit.cb006700.grocery_store.models.UserModel;
import com.apiit.cb006700.grocery_store.models.UsertypeModel;
import com.apiit.cb006700.grocery_store.service.AccountStatusService;
import com.apiit.cb006700.grocery_store.service.UserService;
import com.apiit.cb006700.grocery_store.service.UserTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
public class UserController {

    private static final String SQL_DATE_FORMAT = "yyyy-mm-dd";

    private final UserService userService;
    private final UserTypeService userTypeService;
    private final AccountStatusService accountStatusService;

    @Autowired
    public UserController(UserService userService, UserTypeService userTypeService, AccountStatusService accountStatusService) {
        this.userService = userService;
        this.userTypeService = userTypeService;
        this.accountStatusService = accountStatusService;
    }

    @GetMapping("/users/{email}")
    public UserDTO getUser(@PathVariable String email){
        //TODO:check if user is deleted
        return createUserDTOFromModel(userService.getUserModelByEmail(email));
    }

    @GetMapping("/users")
    @CrossOrigin(origins = "http://localhost:3000")
    public List<UserModel> getUsers(){
        //TODO:check if user is deleted
        return userService.getAllUsers();
    }

    @GetMapping("/users/login/{email}/{password}")
    public boolean userLogin(@PathVariable String email, @PathVariable String password){
        //TODO:check if user is deleted
        return userService.isUserValid(email,password);
    }

    @PostMapping("/users/new")
    public ResponseEntity createUser(@RequestBody UserDTO userDTO) throws ParseException {
        if(userService.isUserExist(userDTO.getEmailAddress())){
            return ResponseEntity.badRequest().body("Email Already In Use");
        }else {
            if (userService.saveUser(createUserModelFromDTO(userDTO))) {
                return ResponseEntity.ok("User Successfully registered");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error When registering user");
            }
        }
    }

    @PostMapping("/users/update")
    public ResponseEntity updateUser(@RequestBody UserDTO userDTO) throws ParseException {
        if (userService.saveUser(createUserModelFromDTO(userDTO))) {
            return ResponseEntity.ok("User Successfully updated");
        }else{
            return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error When updating user");
        }
    }

    @PostMapping("/users/delete/{email}")
    public ResponseEntity deleteUser(@PathVariable("email") String email){
       if (userService.deleteUser(userService.getUserModelByEmail(email))){
           return ResponseEntity.ok("User Successfully deleted");
       }else {
           return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Could Not Delete User");
       }

    }

    private UserModel createUserModelFromDTO(UserDTO userDTO) throws ParseException {
        AccountstatusModel accountstatusModel = accountStatusService.getAccountStatusById(userDTO.getAccountStatusID());
        UsertypeModel usertypeModel = userTypeService.getUserTypeById(userDTO.getUserTypeID());
        Date date = new Date(new SimpleDateFormat(SQL_DATE_FORMAT).parse(userDTO.getDob()).getTime());
        UserModel userModel = new UserModel(userDTO.getFirstName(),userDTO.getLastName(),
                userDTO.getEmailAddress(),userDTO.getPassword(),date,
                usertypeModel,accountstatusModel);
        if (userDTO.getUserID()!= null && !userDTO.getUserID().isEmpty()){
            userModel.setUserId(userDTO.getUserID());
        }
        return userModel;
    }

    private UserDTO createUserDTOFromModel(UserModel userModel){
        return new UserDTO(userModel.getUserId(),userModel.getUsertype().getUserTypeId(),
                userModel.getFirstName(),userModel.getLastName(),userModel.getEmailAddress(),userModel.getPassword(),
                userModel.getDob().toString(),userModel.getAccountstatus().getAccountStatusId());
    }

}
