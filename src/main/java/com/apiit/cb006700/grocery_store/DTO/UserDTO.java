package com.apiit.cb006700.grocery_store.DTO;

public class UserDTO {
    private String userID;
    private String userTypeID;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String password;
    private String dob;
    private String accountStatusID;

    public UserDTO() {
    }

    public UserDTO(String userID, String userTypeID, String firstName, String lastName, String emailAddress, String password, String dob, String accountStatusID) {
        this.userID = userID;
        this.userTypeID = userTypeID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.password = password;
        this.dob = dob;
        this.accountStatusID = accountStatusID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(String userTypeID) {
        this.userTypeID = userTypeID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAccountStatusID() {
        return accountStatusID;
    }

    public void setAccountStatusID(String accountStatusID) {
        this.accountStatusID = accountStatusID;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "userID='" + userID + '\'' +
                ", userTypeID='" + userTypeID + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", password='" + password + '\'' +
                ", dob='" + dob + '\'' +
                ", accountStatusID='" + accountStatusID + '\'' +
                '}';
    }
}
