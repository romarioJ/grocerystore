package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cartitem", schema = "grocerydb_cb006700", catalog = "")
public class CartitemModel {
    private String cartItemId;
    private Integer quantity;
    @JsonBackReference
    private ProductvariationModel productvariation;
    @JsonBackReference
    private CartModel cart;

    @Id
    @GeneratedValue(generator = "cartItem-generator")
    @GenericGenerator(name = "cartItem-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "CIT00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "cartItemID", nullable = false, length = 255)
    public String getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(String cartItemId) {
        this.cartItemId = cartItemId;
    }

    @Basic
    @Column(name = "quantity", nullable = false)
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartitemModel that = (CartitemModel) o;
        return Objects.equals(cartItemId, that.cartItemId) &&
                Objects.equals(quantity, that.quantity);
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "variationID", referencedColumnName = "variationID", nullable = false)
    public ProductvariationModel getProductvariation() {
        return productvariation;
    }

    public void setProductvariation(ProductvariationModel productvariationByVariationId) {
        this.productvariation = productvariationByVariationId;
    }

    @ManyToOne
    @JoinColumn(name = "cartID", referencedColumnName = "cartID", nullable = false)
    public CartModel getCart() {
        return cart;
    }

    public void setCart(CartModel cartByCartId) {
        this.cart = cartByCartId;
    }
}
