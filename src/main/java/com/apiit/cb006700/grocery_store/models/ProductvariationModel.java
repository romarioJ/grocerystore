package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "productvariation", schema = "grocerydb_cb006700", catalog = "")
public class ProductvariationModel {
    private String variationId;
    private String colour;
    private String size;
    private Integer quantity;
//    private Integer price;
    private boolean availability;
    @JsonManagedReference
    private Collection<CartitemModel> cartitems;
    @JsonBackReference
    private ProductModel product;
    @JsonManagedReference
    private Collection<SaleitemModel> saleitems;

    @Id
    @GeneratedValue(generator = "variation-generator")
    @GenericGenerator(name = "variation-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "PV00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "variationID", nullable = false, length = 255)
    public String getVariationId() {
        return variationId;
    }

    public void setVariationId(String variationId) {
        this.variationId = variationId;
    }

    @Basic
    @Column(name = "colour", nullable = false)
    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    @Basic
    @Column(name = "size", nullable = false)
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Basic
    @Column(name = "quantity", nullable = false)
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

//    @Basic
//    @Column(name = "price", nullable = true)
//    public Integer getPrice() {
//        return price;
//    }
//
//    public void setPrice(Integer price) {
//        this.price = price;
//    }

    @Basic
    @Column(name = "availability", nullable = false)
    public boolean getAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductvariationModel that = (ProductvariationModel) o;
        return Objects.equals(variationId, that.variationId) &&
                Objects.equals(colour, that.colour) &&
                Objects.equals(size, that.size) &&
                Objects.equals(quantity, that.quantity) &&
//                Objects.equals(price, that.price) &&
                Objects.equals(availability, that.availability);
    }

    @Override
    public int hashCode() {
        return Objects.hash(variationId, colour, size, quantity, availability);
    }

    @OneToMany(mappedBy = "productvariation")
    public Collection<CartitemModel> getCartitems() {
        return cartitems;
    }

    public void setCartitems(Collection<CartitemModel> cartitemsByVariationId) {
        this.cartitems = cartitemsByVariationId;
    }

    @ManyToOne
    @JoinColumn(name = "productID", referencedColumnName = "productID", nullable = false)
    public ProductModel getProduct() {
        return product;
    }

    public void setProduct(ProductModel productByProductId) {
        this.product = productByProductId;
    }

    @OneToMany(mappedBy = "productvariation")
    public Collection<SaleitemModel> getSaleitems() {
        return saleitems;
    }

    public void setSaleitems(Collection<SaleitemModel> saleitemsByVariationId) {
        this.saleitems = saleitemsByVariationId;
    }
}
