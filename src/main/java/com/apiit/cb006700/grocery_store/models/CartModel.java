package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "cart", schema = "grocerydb_cb006700", catalog = "")
public class CartModel {
    private String cartId;
    private Double cartTotal;
    @JsonBackReference
    private UserModel user;
    @JsonManagedReference
    private Collection<CartitemModel> cartitems;

    @Id
    @GeneratedValue(generator = "cart-generator")
    @GenericGenerator(name = "cart-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "C00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "cartID", nullable = false, length = 255)
    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }


    @Basic
    @Column(name = "cartTotal", nullable = false, precision = 0)
    public Double getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(Double cartTotal) {
        this.cartTotal = cartTotal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartModel cartModel = (CartModel) o;
        return Objects.equals(cartId, cartModel.cartId) &&
                Objects.equals(cartTotal, cartModel.cartTotal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cartId, cartTotal);
    }

    @ManyToOne
    @JoinColumn(name = "userID", referencedColumnName = "userID", nullable = false)
    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel userByUserId) {
        this.user = userByUserId;
    }

    @OneToMany(mappedBy = "cart")
    public Collection<CartitemModel> getCartitems() {
        return cartitems;
    }

    public void setCartitems(Collection<CartitemModel> cartitemsByCartId) {
        this.cartitems = cartitemsByCartId;
    }
}
