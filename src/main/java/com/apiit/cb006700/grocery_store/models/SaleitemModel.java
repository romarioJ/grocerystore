package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "saleitem", schema = "grocerydb_cb006700", catalog = "")
public class SaleitemModel {
    private String saleItemId;
    private Integer quantity;
    private Double saleItemPrice;
    private Double itemDiscountedValue;
    @JsonBackReference
    private ProductvariationModel productvariation;
    @JsonBackReference
    private SaleModel sale;

    @Id
    @GeneratedValue(generator = "saleitem-generator")
    @GenericGenerator(name = "saleitem-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "SI00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "saleItemID", nullable = false, length = 255)
    public String getSaleItemId() {
        return saleItemId;
    }

    public void setSaleItemId(String saleItemId) {
        this.saleItemId = saleItemId;
    }

    @Basic
    @Column(name = "quantity", nullable = false)
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "saleItemPrice", nullable = false, precision = 0)
    public Double getSaleItemPrice() {
        return saleItemPrice;
    }

    public void setSaleItemPrice(Double saleItemPrice) {
        this.saleItemPrice = saleItemPrice;
    }

    @Basic
    @Column(name = "itemDiscountedValue", nullable = false, precision = 0)
    public Double getItemDiscountedValue() {
        return itemDiscountedValue;
    }

    public void setItemDiscountedValue(Double itemDiscountedValue) {
        this.itemDiscountedValue = itemDiscountedValue;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaleitemModel that = (SaleitemModel) o;
        return Objects.equals(saleItemId, that.saleItemId) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(saleItemPrice, that.saleItemPrice) &&
                Objects.equals(itemDiscountedValue, that.itemDiscountedValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(saleItemId, quantity, saleItemPrice, itemDiscountedValue);
    }

    @ManyToOne
    @JoinColumn(name = "variationID", referencedColumnName = "variationID", nullable = false)
    public ProductvariationModel getProductvariation() {
        return productvariation;
    }

    public void setProductvariation(ProductvariationModel productvariationByVariationId) {
        this.productvariation = productvariationByVariationId;
    }

    @ManyToOne
    @JoinColumn(name = "saleID", referencedColumnName = "saleID", nullable = false)
    public SaleModel getSale() {
        return sale;
    }

    public void setSale(SaleModel saleBySaleId) {
        this.sale = saleBySaleId;
    }
}
