package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "discountitem", schema = "grocerydb_cb006700", catalog = "")
public class DiscountitemModel {
    private String discountId;
    private Double discountValue;
    private Timestamp startDate;
    private Timestamp endDate;
    @JsonBackReference
    private ProductModel productModel;

    @Id
    @GeneratedValue(generator = "DiscItem-generator")
    @GenericGenerator(name = "DiscItem-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "DI00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "discountID", nullable = false, length = 255)
    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }

    @Basic
    @Column(name = "discountValue", nullable = false, precision = 0)
    public Double getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(Double discountValue) {
        this.discountValue = discountValue;
    }


    @Basic
    @Column(name = "startDate", nullable = false)
    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "endDate", nullable = false)
    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @OneToOne(mappedBy = "discountitemModel")
    public ProductModel getProductModel() {
        return productModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscountitemModel that = (DiscountitemModel) o;
        return Objects.equals(discountId, that.discountId) &&
                Objects.equals(discountValue, that.discountValue) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(discountId, discountValue, startDate, endDate);
    }



    public void setProductModel(ProductModel productModel) {
        this.productModel = productModel;
    }
}
