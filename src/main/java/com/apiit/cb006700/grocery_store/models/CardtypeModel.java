package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "cardtype", schema = "grocerydb_cb006700", catalog = "")
public class CardtypeModel {
    private String cardTypeId;
    private String cardTypeDesc;
    @JsonManagedReference
    private Collection<CreditcardModel> creditcards;

    @Id
    @GeneratedValue(generator = "cardType-generator")
    @GenericGenerator(name = "cardType-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "CT00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "cardTypeID", nullable = false, length = 255)
    public String getCardTypeId() {
        return cardTypeId;
    }

    public void setCardTypeId(String cardTypeId) {
        this.cardTypeId = cardTypeId;
    }

    @Basic
    @Column(name = "cardTypeDesc", nullable = false, length = 255)
    public String getCardTypeDesc() {
        return cardTypeDesc;
    }

    public void setCardTypeDesc(String cardTypeDesc) {
        this.cardTypeDesc = cardTypeDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardtypeModel that = (CardtypeModel) o;
        return Objects.equals(cardTypeId, that.cardTypeId) &&
                Objects.equals(cardTypeDesc, that.cardTypeDesc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardTypeId, cardTypeDesc);
    }

    @OneToMany(mappedBy = "cardtype")
    public Collection<CreditcardModel> getCreditcards() {
        return creditcards;
    }

    public void setCreditcards(Collection<CreditcardModel> creditcardsByCardTypeId) {
        this.creditcards = creditcardsByCardTypeId;
    }
}
