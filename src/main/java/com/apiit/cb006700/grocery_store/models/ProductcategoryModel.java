package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "productcategory", schema = "grocerydb_cb006700", catalog = "")
public class ProductcategoryModel {

    private String productCategoryId;
    private String productCatDesc;
    private String productCatImg;
    private String productCatName;
//    @JsonManagedReference
//    private Collection<ProductModel> products;
    @JsonManagedReference
    private Collection<SubCategoryModel> subcategories;

    @Id
    @GeneratedValue(generator = "category-generator")
    @GenericGenerator(name = "category-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "CAT00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "productCategoryID", nullable = false, length = 255)
    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    @Basic
    @Column(name = "productCatDesc", nullable = false, length = 255)
    public String getProductCatDesc() {
        return productCatDesc;
    }

    public void setProductCatDesc(String productCatDesc) {
        this.productCatDesc = productCatDesc;
    }

    @Basic
    @Column(name = "productCatImg", nullable = false, length = 255)
    public String getProductCatImg() {
        return productCatImg;
    }

    public void setProductCatImg(String productCatImg) {
        this.productCatImg = productCatImg;
    }

    @Basic
    @Column(name = "productCatName", nullable = false, length = 255)
    public String getProductCatName() {
        return productCatName;
    }

    public void setProductCatName(String productCatName) {
        this.productCatName = productCatName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductcategoryModel that = (ProductcategoryModel) o;
        return Objects.equals(productCategoryId, that.productCategoryId) &&
                Objects.equals(productCatDesc, that.productCatDesc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productCategoryId, productCatDesc);
    }

//    @OneToMany(mappedBy = "productcategory")
//    public Collection<ProductModel> getProducts() {
//        return products;
//    }
//
//    public void setProducts(Collection<ProductModel> productsByProductCategoryId) {
//        this.products = productsByProductCategoryId;
//    }

    @OneToMany(mappedBy = "productcategory")
    public Collection<SubCategoryModel> getSubCategoryModels() {
        return subcategories;
    }

    public void setSubCategoryModels(Collection<SubCategoryModel> subCategoryModels) {
        this.subcategories = subCategoryModels;
    }
}
