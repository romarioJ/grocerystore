package com.apiit.cb006700.grocery_store.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "user", schema = "grocerydb_cb006700", catalog = "")
public class UserModel {
    private String userId;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String password;
    private Date dob;
    @JsonManagedReference
    private Collection<AddressModel> addresses;
    @JsonManagedReference
    private Collection<CartModel> carts;
    @JsonManagedReference
    private Collection<CreditcardModel> creditcards;
    @JsonManagedReference
    private Collection<SaleModel> sales;
    @JsonBackReference
    private UsertypeModel usertype;
    @JsonBackReference
    private AccountstatusModel accountstatus;

    public UserModel() {
    }

    public UserModel( String firstName, String lastName, String emailAddress, String password, Date dob,
                     UsertypeModel usertypeByUserTypeId, AccountstatusModel accountstatusByAccountStatusId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.password = password;
        this.dob = dob;
        this.usertype = usertypeByUserTypeId;
        this.accountstatus = accountstatusByAccountStatusId;
    }

    @Id
    @GeneratedValue(generator = "user-generator")
    @GenericGenerator(name = "user-generator", parameters = @Parameter(name = "prefix", value = "U00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "userID", nullable = false, length = 255)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "firstName", nullable = false, length = 255)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "lastName", nullable = false, length = 255)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "emailAddress", nullable = false, length = 255)
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 255)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "dob", nullable = false)
    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserModel userModel = (UserModel) o;
        return Objects.equals(userId, userModel.userId) &&
                Objects.equals(firstName, userModel.firstName) &&
                Objects.equals(lastName, userModel.lastName) &&
                Objects.equals(emailAddress, userModel.emailAddress) &&
                Objects.equals(password, userModel.password) &&
                Objects.equals(dob, userModel.dob);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, firstName, lastName, emailAddress, password, dob);
    }

    @OneToMany(mappedBy = "user")
    public Collection<AddressModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(Collection<AddressModel> addressesByUserId) {
        this.addresses = addressesByUserId;
    }

    @OneToMany(mappedBy = "user")
    public Collection<CartModel> getCarts() {
        return carts;
    }

    public void setCarts(Collection<CartModel> cartsByUserId) {
        this.carts = cartsByUserId;
    }

    @OneToMany(mappedBy = "user")
    public Collection<CreditcardModel> getCreditcards() {
        return creditcards;
    }

    public void setCreditcards(Collection<CreditcardModel> creditcardsByUserId) {
        this.creditcards = creditcardsByUserId;
    }

    @OneToMany(mappedBy = "user")
    public Collection<SaleModel> getSales() {
        return sales;
    }

    public void setSales(Collection<SaleModel> salesByUserId) {
        this.sales = salesByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "userTypeID", referencedColumnName = "userTypeID", nullable = false)
    public UsertypeModel getUsertype() {
        return usertype;
    }

    public void setUsertype(UsertypeModel usertypeByUserTypeId) {
        this.usertype = usertypeByUserTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "accountStatusID", referencedColumnName = "accountStatusID", nullable = false)
    public AccountstatusModel getAccountstatus() {
        return accountstatus;
    }

    public void setAccountstatus(AccountstatusModel accountstatusByAccountStatusId) {
        this.accountstatus = accountstatusByAccountStatusId;
    }
}
