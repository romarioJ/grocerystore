package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "product", schema = "grocerydb_cb006700", catalog = "")
public class ProductModel {
    private String productId;
    private String productName;
    private String productImage;
    private String productDesc;
    private Integer productReorderLevel;
    private Double productPrice;
    private boolean productAvailability;
//    @JsonBackReference
//    private ProductcategoryModel productcategory;
    @JsonBackReference
    private ProductbrandModel productbrand;
    @JsonManagedReference
    private Collection<ProductvariationModel> productvariations;
    @JsonManagedReference
    private Collection<ReviewModel> reviews;
    @JsonBackReference
    private SubCategoryModel subcategory;
    @JsonManagedReference
    private DiscountitemModel discountitemModel;


    @Id
    @GeneratedValue(generator = "prod-generator")
    @GenericGenerator(name = "prod-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "P00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "productID", nullable = false, length = 255)
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "productName", nullable = false, length = 255)
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Basic
    @Column(name = "productImage", nullable = false, length = 255)
    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }


    @Basic
    @Column(name = "productDesc", nullable = false, length = 255)
    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    @Basic
    @Column(name = "productReorderLevel", nullable = false)
    public Integer getProductReorderLevel() {
        return productReorderLevel;
    }

    public void setProductReorderLevel(Integer productReorderLevel) {
        this.productReorderLevel = productReorderLevel;
    }

    @Basic
    @Column(name = "productAvailability", nullable = false, length = 255)
    public boolean getProductAvailability() {
        return productAvailability;
    }

    public void setProductAvailability(boolean productAvailability) {
        this.productAvailability = productAvailability;
    }

    @Basic
    @Column(name = "productPrice", nullable = false, length = 255)
    public Double getPrice() {
        return productPrice;
    }

    public void setPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductModel that = (ProductModel) o;
        return Objects.equals(productId, that.productId) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(productImage, that.productImage) &&
                Objects.equals(productAvailability, that.productAvailability) &&
                Objects.equals(productDesc, that.productDesc) &&
                Objects.equals(productReorderLevel, that.productReorderLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, productName, productImage,productAvailability, productDesc, productReorderLevel);
    }

//    @ManyToOne
//    @JoinColumn(name = "productCatID", referencedColumnName = "productCategoryID", nullable = false)
//    public ProductcategoryModel getProductcategory() {
//        return productcategory;
//    }
//
//    public void setProductcategory(ProductcategoryModel productcategoryByProductCatId) {
//        this.productcategory = productcategoryByProductCatId;
//    }

    @ManyToOne
    @JoinColumn(name = "productBrandID", referencedColumnName = "productBrandID", nullable = false)
    public ProductbrandModel getProductbrand() {
        return productbrand;
    }

    public void setProductbrand(ProductbrandModel productbrandByProductBrandId) {
        this.productbrand = productbrandByProductBrandId;
    }

    @OneToMany(mappedBy = "product")
    public Collection<ProductvariationModel> getProductvariations() {
        return productvariations;
    }

    public void setProductvariations(Collection<ProductvariationModel> productvariationsByProductId) {
        this.productvariations = productvariationsByProductId;
    }

    @OneToOne
    @JoinColumn(name = "discountItemID", referencedColumnName = "discountID")
    public DiscountitemModel getDiscountitemModel() {
        return discountitemModel;
    }

    public void setDiscountitemModel(DiscountitemModel discountitemModel) {
        this.discountitemModel = discountitemModel;
    }

    @OneToMany(mappedBy = "product")
    public Collection<ReviewModel> getReviews() {
        return reviews;
    }

    public void setReviews(Collection<ReviewModel> reviewsByProductId) {
        this.reviews = reviewsByProductId;
    }


    @ManyToOne
    @JoinColumn(name = "subCatID", referencedColumnName = "subCatID")
    public SubCategoryModel getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(SubCategoryModel subCategoryModel) {
        this.subcategory = subCategoryModel;
    }
}
