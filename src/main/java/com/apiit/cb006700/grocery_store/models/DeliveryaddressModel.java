package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "deliveryaddress", schema = "grocerydb_cb006700", catalog = "")
public class DeliveryaddressModel {
    private String deliveryId;
    @JsonBackReference
    private SaleModel sale;
    @JsonBackReference
    private AddressModel address;

    @Id
    @GeneratedValue(generator = "DelAdd-generator")
    @GenericGenerator(name = "DelAdd-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "DA00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "deliveryID", nullable = false, length = 255)
    public String getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(String deliveryId) {
        this.deliveryId = deliveryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliveryaddressModel that = (DeliveryaddressModel) o;
        return Objects.equals(deliveryId, that.deliveryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deliveryId);
    }

    @ManyToOne
    @JoinColumn(name = "saleID", referencedColumnName = "saleID", nullable = false)
    public SaleModel getSale() {
        return sale;
    }

    public void setSale(SaleModel saleBySaleId) {
        this.sale = saleBySaleId;
    }

    @ManyToOne
    @JoinColumn(name = "addressID", referencedColumnName = "adressID", nullable = false)
    public AddressModel getAddress() {
        return address;
    }

    public void setAddress(AddressModel addressByAddressId) {
        this.address = addressByAddressId;
    }
}
