package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "sale", schema = "grocerydb_cb006700", catalog = "")
public class SaleModel {
    private String saleId;
    private Timestamp saleDate;
    private Double saleValue;
    private Double discountedValue;
    @JsonManagedReference
    private Collection<CardpaymentsModel> cardpayments;
    @JsonManagedReference
    private Collection<DeliveryaddressModel> deliveryaddresses;
    @JsonBackReference
    private SaletypeModel saletype;
    @JsonBackReference
    private UserModel user;
    @JsonBackReference
    private SalestatusModel salestatus;
    @JsonManagedReference
    private Collection<SaleitemModel> saleitems;

    @Id
    @GeneratedValue(generator = "sale-generator")
    @GenericGenerator(name = "sale-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "S00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "saleID", nullable = false, length = 255)
    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    @Basic
    @Column(name = "saleDate", nullable = false)
    public Timestamp getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Timestamp saleDate) {
        this.saleDate = saleDate;
    }

    @Basic
    @Column(name = "saleValue", nullable = false, precision = 0)
    public Double getSaleValue() {
        return saleValue;
    }

    public void setSaleValue(Double saleValue) {
        this.saleValue = saleValue;
    }

    @Basic
    @Column(name = "discountedValue", nullable = false, precision = 0)
    public Double getDiscountedValue() {
        return discountedValue;
    }

    public void setDiscountedValue(Double discountedValue) {
        this.discountedValue = discountedValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaleModel saleModel = (SaleModel) o;
        return Objects.equals(saleId, saleModel.saleId) &&
                Objects.equals(saleDate, saleModel.saleDate) &&
                Objects.equals(saleValue, saleModel.saleValue) &&
                Objects.equals(discountedValue, saleModel.discountedValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(saleId, saleDate,saleValue, discountedValue);
    }

    @OneToMany(mappedBy = "sale")
    public Collection<CardpaymentsModel> getCardpayments() {
        return cardpayments;
    }

    public void setCardpayments(Collection<CardpaymentsModel> cardpaymentsBySaleId) {
        this.cardpayments = cardpaymentsBySaleId;
    }

    @OneToMany(mappedBy = "sale")
    public Collection<DeliveryaddressModel> getDeliveryaddresses() {
        return deliveryaddresses;
    }

    public void setDeliveryaddresses(Collection<DeliveryaddressModel> deliveryaddressesBySaleId) {
        this.deliveryaddresses = deliveryaddressesBySaleId;
    }

    @ManyToOne
    @JoinColumn(name = "saleTypeID", referencedColumnName = "saleTypeID", nullable = false)
    public SaletypeModel getSaletype() {
        return saletype;
    }

    public void setSaletype(SaletypeModel saletypeBySaleTypeId) {
        this.saletype = saletypeBySaleTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "userID", referencedColumnName = "userID", nullable = false)
    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel userByUserId) {
        this.user = userByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "saleStatusID", referencedColumnName = "saleStatusID", nullable = false)
    public SalestatusModel getSalestatus() {
        return salestatus;
    }

    public void setSalestatus(SalestatusModel salestatusBySaleStatusId) {
        this.salestatus = salestatusBySaleStatusId;
    }

    @OneToMany(mappedBy = "sale")
    public Collection<SaleitemModel> getSaleitems() {
        return saleitems;
    }

    public void setSaleitems(Collection<SaleitemModel> saleitemsBySaleId) {
        this.saleitems = saleitemsBySaleId;
    }
}
