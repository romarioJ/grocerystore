package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "usertype", schema = "grocerydb_cb006700", catalog = "")
public class UsertypeModel {
    private String userTypeId;
    private String userTypeDesc;
    @JsonManagedReference
    private Collection<UserModel> users;

    @Id
    @GeneratedValue(generator = "userType-generator")
    @GenericGenerator(name = "userType-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "UT00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "userTypeID", nullable = false, length = 255)
    public String getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(String userTypeId) {
        this.userTypeId = userTypeId;
    }

    @Basic
    @Column(name = "userTypeDesc", nullable = false, length = 255)
    public String getUserTypeDesc() {
        return userTypeDesc;
    }

    public void setUserTypeDesc(String userTypeDesc) {
        this.userTypeDesc = userTypeDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsertypeModel that = (UsertypeModel) o;
        return Objects.equals(userTypeId, that.userTypeId) &&
                Objects.equals(userTypeDesc, that.userTypeDesc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userTypeId, userTypeDesc);
    }

    @OneToMany(mappedBy = "usertype")
    public Collection<UserModel> getUsers() {
        return users;
    }

    public void setUsers(Collection<UserModel> usersByUserTypeId) {
        this.users = usersByUserTypeId;
    }
}
