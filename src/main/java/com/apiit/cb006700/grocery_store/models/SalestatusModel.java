package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "salestatus", schema = "grocerydb_cb006700", catalog = "")
public class SalestatusModel {
    private String saleStatusId;
    private String saleStatusDesc;
    @JsonManagedReference
    private Collection<SaleModel> sales;

    @Id
    @GeneratedValue(generator = "saleStatus-generator")
    @GenericGenerator(name = "saleStatus-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "SS00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "saleStatusID", nullable = false, length = 255)
    public String getSaleStatusId() {
        return saleStatusId;
    }

    public void setSaleStatusId(String saleStatusId) {
        this.saleStatusId = saleStatusId;
    }

    @Basic
    @Column(name = "saleStatusDesc", nullable = false, length = 255)
    public String getSaleStatusDesc() {
        return saleStatusDesc;
    }

    public void setSaleStatusDesc(String saleStatusDesc) {
        this.saleStatusDesc = saleStatusDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SalestatusModel that = (SalestatusModel) o;
        return Objects.equals(saleStatusId, that.saleStatusId) &&
                Objects.equals(saleStatusDesc, that.saleStatusDesc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(saleStatusId, saleStatusDesc);
    }

    @OneToMany(mappedBy = "salestatus")
    public Collection<SaleModel> getSales() {
        return sales;
    }

    public void setSales(Collection<SaleModel> salesBySaleStatusId) {
        this.sales = salesBySaleStatusId;
    }
}
