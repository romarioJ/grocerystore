package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "productbrand", schema = "grocerydb_cb006700", catalog = "")
public class ProductbrandModel {
    private String productBrandId;
    private String productBrandDesc;
    private String productBrandImg;
    @JsonManagedReference
    private Collection<ProductModel> products;

    @Id
    @GeneratedValue(generator = "brand-generator")
    @GenericGenerator(name = "brand-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "B00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "productBrandID", nullable = false, length = 255)
    public String getProductBrandId() {
        return productBrandId;
    }

    public void setProductBrandId(String productBrandId) {
        this.productBrandId = productBrandId;
    }

    @Basic
    @Column(name = "productBrandDesc", nullable = false, length = 255)
    public String getProductBrandDesc() {
        return productBrandDesc;
    }

    public void setProductBrandDesc(String productBrandDesc) {
        this.productBrandDesc = productBrandDesc;
    }

    @Basic
    @Column(name = "productBrandImg", nullable = false, length = 255)
    public String getProductBrandImg() {
        return productBrandImg;
    }

    public void setProductBrandImg(String productBrandImg) {
        this.productBrandImg = productBrandImg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductbrandModel that = (ProductbrandModel) o;
        return Objects.equals(productBrandId, that.productBrandId) &&
                Objects.equals(productBrandDesc, that.productBrandDesc) &&
                Objects.equals(productBrandImg, that.productBrandImg);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productBrandId, productBrandDesc, productBrandImg);
    }

    @OneToMany(mappedBy = "productbrand")
    public Collection<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Collection<ProductModel> productsByProductBrandId) {
        this.products = productsByProductBrandId;
    }
}
