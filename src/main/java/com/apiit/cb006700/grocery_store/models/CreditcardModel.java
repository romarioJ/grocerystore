package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "creditcard", schema = "grocerydb_cb006700", catalog = "")
public class CreditcardModel {
    private String cardId;
    private String cardNumber;
    private Integer cardCvv;
    private Date expiaryDate;
    @JsonManagedReference
    private Collection<CardpaymentsModel> cardpayments;
    @JsonBackReference
    private UserModel user;
    @JsonBackReference
    private CardtypeModel cardtype;

    @Id
    @GeneratedValue(generator = "card-generator")
    @GenericGenerator(name = "card-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "CC00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "cardID", nullable = false, length = 255)
    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    @Basic
    @Column(name = "cardNumber", nullable = false, length = 255)
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "cardCVV", nullable = false)
    public Integer getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(Integer cardCvv) {
        this.cardCvv = cardCvv;
    }

    @Basic
    @Column(name = "expiaryDate", nullable = false)
    public Date getExpiaryDate() {
        return expiaryDate;
    }

    public void setExpiaryDate(Date expiaryDate) {
        this.expiaryDate = expiaryDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditcardModel that = (CreditcardModel) o;
        return Objects.equals(cardId, that.cardId) &&
                Objects.equals(cardNumber, that.cardNumber) &&
                Objects.equals(cardCvv, that.cardCvv) &&
                Objects.equals(expiaryDate, that.expiaryDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardId, cardNumber, cardCvv, expiaryDate);
    }

    @OneToMany(mappedBy = "creditcard")
    public Collection<CardpaymentsModel> getCardpayments() {
        return cardpayments;
    }

    public void setCardpayments(Collection<CardpaymentsModel> cardpaymentsByCardId) {
        this.cardpayments = cardpaymentsByCardId;
    }

    @ManyToOne
    @JoinColumn(name = "userID", referencedColumnName = "userID", nullable = false)
    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel userByUserId) {
        this.user = userByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "cardTypeID", referencedColumnName = "cardTypeID", nullable = false)
    public CardtypeModel getCardtype() {
        return cardtype;
    }

    public void setCardtype(CardtypeModel cardtypeByCardTypeId) {
        this.cardtype = cardtypeByCardTypeId;
    }
}
