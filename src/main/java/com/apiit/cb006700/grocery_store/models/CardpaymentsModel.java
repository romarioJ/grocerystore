package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cardpayments", schema = "grocerydb_cb006700", catalog = "")
public class CardpaymentsModel {
    private String cardPaymentId;
    @JsonBackReference
    private CreditcardModel creditcard;
    @JsonBackReference
    private SaleModel sale;

    @Id
    @GeneratedValue(generator = "cardPay-generator")
    @GenericGenerator(name = "cardPay-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "CP00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "cardPaymentID", nullable = false, length = 255)
    public String getCardPaymentId() {
        return cardPaymentId;
    }

    public void setCardPaymentId(String cardPaymentId) {
        this.cardPaymentId = cardPaymentId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardpaymentsModel that = (CardpaymentsModel) o;
        return Objects.equals(cardPaymentId, that.cardPaymentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardPaymentId);
    }

    @ManyToOne
    @JoinColumn(name = "cardID", referencedColumnName = "cardID", nullable = false)
    public CreditcardModel getCreditcard() {
        return creditcard;
    }

    public void setCreditcard(CreditcardModel creditcardByCardId) {
        this.creditcard = creditcardByCardId;
    }

    @ManyToOne
    @JoinColumn(name = "saleID", referencedColumnName = "saleID", nullable = false)
    public SaleModel getSale() {
        return sale;
    }

    public void setSale(SaleModel saleBySaleId) {
        this.sale = saleBySaleId;
    }
}
