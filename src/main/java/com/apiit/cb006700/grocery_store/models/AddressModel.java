package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "address", schema = "grocerydb_cb006700", catalog = "")
public class AddressModel {
    private String adressId;
    private String addressLine1;
    private String adressLine2;
    private String city;
    private String postCode;
    private String contactNumber;
    @JsonBackReference
    private UserModel user;
    @JsonManagedReference
    private Collection<DeliveryaddressModel> deliveryaddresses;

    @Id
    @GeneratedValue(generator = "address-generator")
    @GenericGenerator(name = "address-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "ADD00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "adressID", nullable = false, length = 255)
    public String getAdressId() {
        return adressId;
    }

    public void setAdressId(String adressId) {
        this.adressId = adressId;
    }

    @Basic
    @Column(name = "addressLine1", nullable = false, length = 255)
    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    @Basic
    @Column(name = "adressLine2", nullable = false, length = 255)
    public String getAdressLine2() {
        return adressLine2;
    }

    public void setAdressLine2(String adressLine2) {
        this.adressLine2 = adressLine2;
    }

    @Basic
    @Column(name = "city", nullable = false, length = 255)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "postCode", nullable = false, length = 10)
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Basic
    @Column(name = "contactNumber", nullable = false, length = 255)
    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressModel that = (AddressModel) o;
        return Objects.equals(adressId, that.adressId) &&
                Objects.equals(addressLine1, that.addressLine1) &&
                Objects.equals(adressLine2, that.adressLine2) &&
                Objects.equals(city, that.city) &&
                Objects.equals(postCode, that.postCode) &&
                Objects.equals(contactNumber, that.contactNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adressId, addressLine1, adressLine2, city, postCode, contactNumber);
    }

    @ManyToOne
    @JoinColumn(name = "userID", referencedColumnName = "userID", nullable = false)
    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel userByUserId) {
        this.user = userByUserId;
    }

    @OneToMany(mappedBy = "address")
    public Collection<DeliveryaddressModel> getDeliveryaddresses() {
        return deliveryaddresses;
    }

    public void setDeliveryaddresses(Collection<DeliveryaddressModel> deliveryaddressesByAdressId) {
        this.deliveryaddresses = deliveryaddressesByAdressId;
    }
}
