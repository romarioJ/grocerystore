package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "review", schema = "grocerydb_cb006700", catalog = "")
public class ReviewModel {
    private String reviewId;
    private Integer reviewRating;
    private String userId;
    private String reviewDesc;
    private Timestamp reviewDate;
    private Byte reviewStatus;
    @JsonManagedReference
    private Collection<ReplyModel> replies;
    @JsonBackReference
    private ProductModel product;

    @Id
    @GeneratedValue(generator = "rev-generator")
    @GenericGenerator(name = "rev-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "R00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "reviewID", nullable = false, length = 255)
    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    @Basic
    @Column(name = "reviewRating", nullable = false)
    public Integer getReviewRating() {
        return reviewRating;
    }

    public void setReviewRating(Integer reviewRating) {
        this.reviewRating = reviewRating;
    }

    @Basic
    @Column(name = "userID", nullable = false, length = 255)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "reviewDesc", nullable = false, length = 255)
    public String getReviewDesc() {
        return reviewDesc;
    }

    public void setReviewDesc(String reviewDesc) {
        this.reviewDesc = reviewDesc;
    }

    @Basic
    @Column(name = "reviewDate", nullable = false)
    public Timestamp getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Timestamp reviewDate) {
        this.reviewDate = reviewDate;
    }

    @Basic
    @Column(name = "reviewStatus", nullable = false)
    public Byte getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(Byte reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReviewModel that = (ReviewModel) o;
        return Objects.equals(reviewId, that.reviewId) &&
                Objects.equals(reviewRating, that.reviewRating) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(reviewDesc, that.reviewDesc) &&
                Objects.equals(reviewDate, that.reviewDate) &&
                Objects.equals(reviewStatus, that.reviewStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reviewId, reviewRating, userId, reviewDesc, reviewDate, reviewStatus);
    }

    @OneToMany(mappedBy = "review")
    public Collection<ReplyModel> getReplies() {
        return replies;
    }

    public void setReplies(Collection<ReplyModel> repliesByReviewId) {
        this.replies = repliesByReviewId;
    }

    @ManyToOne
    @JoinColumn(name = "productID", referencedColumnName = "productID", nullable = false)
    public ProductModel getProduct() {
        return product;
    }

    public void setProduct(ProductModel productByProductId) {
        this.product = productByProductId;
    }
}
