package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "saletype", schema = "grocerydb_cb006700", catalog = "")
public class SaletypeModel {
    private String saleTypeId;
    private String saleTypeDesc;
    @JsonManagedReference
    private Collection<SaleModel> sales;

    @Id
    @GeneratedValue(generator = "saleType-generator")
    @GenericGenerator(name = "saleType-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "ST00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "saleTypeID", nullable = false, length = 255)
    public String getSaleTypeId() {
        return saleTypeId;
    }

    public void setSaleTypeId(String saleTypeId) {
        this.saleTypeId = saleTypeId;
    }

    @Basic
    @Column(name = "saleTypeDesc", nullable = false, length = 255)
    public String getSaleTypeDesc() {
        return saleTypeDesc;
    }

    public void setSaleTypeDesc(String saleTypeDesc) {
        this.saleTypeDesc = saleTypeDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaletypeModel that = (SaletypeModel) o;
        return Objects.equals(saleTypeId, that.saleTypeId) &&
                Objects.equals(saleTypeDesc, that.saleTypeDesc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(saleTypeId, saleTypeDesc);
    }

    @OneToMany(mappedBy = "saletype")
    public Collection<SaleModel> getSales() {
        return sales;
    }

    public void setSales(Collection<SaleModel> salesBySaleTypeId) {
        this.sales = salesBySaleTypeId;
    }
}
