package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "reply", schema = "grocerydb_cb006700", catalog = "")
public class ReplyModel {
    private String replyId;
    private String userId;
    private String replyDesc;
    @JsonBackReference
    private ReviewModel review;

    @Id
    @GeneratedValue(generator = "reply-generator")
    @GenericGenerator(name = "reply-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "REP00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "replyID", nullable = false, length = 255)
    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    @Basic
    @Column(name = "userID", nullable = false, length = 255)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "replyDesc", nullable = false, length = 255)
    public String getReplyDesc() {
        return replyDesc;
    }

    public void setReplyDesc(String replyDesc) {
        this.replyDesc = replyDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReplyModel that = (ReplyModel) o;
        return Objects.equals(replyId, that.replyId) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(replyDesc, that.replyDesc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(replyId, userId, replyDesc);
    }

    @ManyToOne
    @JoinColumn(name = "reviewID", referencedColumnName = "reviewID", nullable = false)
    public ReviewModel getReview() {
        return review;
    }

    public void setReview(ReviewModel reviewByReviewId) {
        this.review = reviewByReviewId;
    }
}
