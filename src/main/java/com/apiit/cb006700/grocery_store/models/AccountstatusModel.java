package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "accountstatus", schema = "grocerydb_cb006700", catalog = "")
public class  AccountstatusModel {
    private String accountStatusId;
    private String accountStatusDesc;
    @JsonManagedReference
    private Collection<UserModel> users;

    @Id
    @GeneratedValue(generator = "acstat-generator")
    @GenericGenerator(name = "acstat-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "STAT00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "accountStatusID", nullable = false, length = 255)
    public String getAccountStatusId() {
        return accountStatusId;
    }

    public void setAccountStatusId(String accountStatusId) {
        this.accountStatusId = accountStatusId;
    }

    @Basic
    @Column(name = "accountStatusDesc", nullable = false, length = 255)
    public String getAccountStatusDesc() {
        return accountStatusDesc;
    }

    public void setAccountStatusDesc(String accountStatusDesc) {
        this.accountStatusDesc = accountStatusDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountstatusModel that = (AccountstatusModel) o;
        return Objects.equals(accountStatusId, that.accountStatusId) &&
                Objects.equals(accountStatusDesc, that.accountStatusDesc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountStatusId, accountStatusDesc);
    }

    @OneToMany(mappedBy = "accountstatus")
    public Collection<UserModel> getUsers() {
        return users;
    }

    public void setUsers(Collection<UserModel> usersByAccountStatusId) {
        this.users = usersByAccountStatusId;
    }
}
