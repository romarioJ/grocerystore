package com.apiit.cb006700.grocery_store.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "subcategory", schema = "grocerydb_cb006700", catalog = "")
public class SubCategoryModel {
    private String subCatID;
    private String subCatName;
    @JsonBackReference
    private ProductcategoryModel productcategory;
    @JsonManagedReference
    private Collection<ProductModel> products;

    @Id
    @GeneratedValue(generator = "subCat-generator")
    @GenericGenerator(name = "subCat-generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "SCAT00"),
            strategy = "com.apiit.cb006700.grocery_store.models.generator.CustomGenerator")
    @Column(name = "subCatID", nullable = false, length = 255)
    public String getSubCatID() {
        return subCatID;
    }
    public void setSubCatID(String subCatID) {
        this.subCatID = subCatID;
    }

    @Basic
    @Column(name = "subCatName", nullable = false, length = 255)
    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    @OneToMany(mappedBy = "subcategory")
    public Collection<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Collection<ProductModel> products) {
        this.products = products;
    }

    @ManyToOne
    @JoinColumn(name = "productCategoryID", referencedColumnName = "productCategoryID")
    public ProductcategoryModel getProductcategory() {
        return productcategory;
    }

    public void setProductcategory(ProductcategoryModel productcategory) {
        this.productcategory = productcategory;
    }
}
